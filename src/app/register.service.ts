import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Register } from './register.model';
import { map, tap } from 'rxjs/operators';


@Injectable()
export class RegisterService {
  registerUpLoading = new Subject<boolean>();
  registerChange = new Subject<Register[]>();
  registerFetching = new Subject<boolean>();
  registerRemoving = new Subject<boolean>();
  private registers: Register[] = [];

  constructor(private http: HttpClient){}


  fetchRegisters(){
    this.registerFetching.next(true);
    this.http.get<{[id: string]: Register}>('https://alina-beaf9-default-rtdb.firebaseio.com/register.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const formData = result[id];
          return new Register(
            id,
            formData.name,
            formData.lastName,
            formData.surname,
            formData.phone,
            formData.work,
            formData.gender,
            formData.size,
            formData.comment,
            formData.skills
          );
        })
      })).subscribe(registers => {
      this.registers = registers;
      this.registerChange.next(this.registers.slice());
      this.registerFetching.next(false);
    }, error => {
      this.registerFetching.next(false);
    })
  }

  getForm(register: Register){
    const body = {
      name: register.name,
      lastName: register.lastName,
      surname:  register.surname,
      phone: register.phone,
      work: register.work,
      gender: register.gender,
      size: register.size,
      comment: register.comment,
      skills: register.skills,
    };

    this.registerUpLoading.next(true);
    return this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/register.json', body).pipe(
      tap(() => {
        this.registerUpLoading.next(false);
      }, () => {
        this.registerUpLoading.next(false);
      })
    )
  }

  getRegister(){
    return this.registers.slice();
  }

  fetchRegister(id: string){
    return this.http.get<Register | null >(`https://alina-beaf9-default-rtdb.firebaseio.com/register/${id}.json`).pipe(
      map(result => {
        if (!result){
          return null;
        }
        return new Register(
          id,result.name,
          result.lastName,
          result.surname,
          result.phone,
          result.work,
          result.gender,
          result.size,
          result.comment,
          result.skills);
      })
    );
  }

  editRegister(register: Register){
    this.registerUpLoading.next(true);
    const body = {
      name: register.name,
      lastName: register.lastName,
      surname:  register.surname,
      phone: register.phone,
      work: register.work,
      gender: register.gender,
      size: register.size,
      comment: register.comment,
      skills: register.skills,

    };

    return this.http.put(`https://alina-beaf9-default-rtdb.firebaseio.com/register/${register.id}.json`, body).pipe(
      tap(() => {
        this.registerUpLoading.next(false);
      }, () => {
        this.registerUpLoading.next(false);
      })
    );
  }

  removeRegister(id: string){
    this.registerRemoving.next(true);
    return this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/register/${id}.json`).pipe(
      tap(() => {
        this.registerRemoving.next(false);
      }, () => {
        this.registerRemoving.next(false);
      })
    )
  }



}
