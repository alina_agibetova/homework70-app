import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { ApplicationsComponent } from './applications/applications.component';
import { AnswerFormComponent } from './answer-form.component';
import { RegisterResolverService } from './register/register-resolver.service';

const routes: Routes = [
  {path: '', component: RegisterComponent},
  {path: 'app-register', component: AnswerFormComponent},
  {path:':id/register', component: RegisterComponent, resolve: {register: RegisterResolverService}},
  {path: 'applications', component: ApplicationsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
