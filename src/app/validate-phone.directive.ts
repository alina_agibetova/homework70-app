
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const phoneValidator = (control: AbstractControl): ValidationErrors | null => {
  // control.value
  const hasPhone = /[+][(][0-9]{3}[)][0-9]{3}[-][0-9]{2}[-][0-9]{2}[-][0-9]{2}/.test(control.value);


  if (hasPhone) {
    return null;
  }

  return {phone: true};
};

@Directive({
  selector: '[appPhone]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePhoneDirective,
    multi: true
  }]
})
export class ValidatePhoneDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return phoneValidator(control);
  }
}
