import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { Register, Skill } from '../register.model';
import { phoneValidator } from '../validate-phone.directive';

@Component({
  selector: 'app-forms',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  isUpLoading = false;
  registerUpLoadingSubscription!: Subscription;
  editedId = '';
  registers!: Register;
  isEdit = false;
  isClick = true;
  profileForm!: FormGroup;

  constructor(private router: Router, private registerService: RegisterService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.profileForm = new FormGroup({
      name: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      surname: new FormControl('', Validators.required),
      phone: new FormControl('', [Validators.required, Validators.minLength(18), phoneValidator]),
      work: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      comment: new FormControl('', Validators.required),
      skills: new FormArray([])
    });
    this.registerUpLoadingSubscription = this.registerService.registerUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });

    this.route.data.subscribe(data => {
      const register = <Register | null> data.register;
      this.registers = data.register;
      if (register){
        this.isEdit = true;
        this.editedId = register.id;
        this.setFormValue({
          name: register.name,
          lastName: register.lastName,
          surname: register.surname,
          phone: register.phone,
          work: register.work,
          gender: register.gender,
          size: register.size,
          comment: register.comment,
          skills: [],

        })
      }  else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          name: '',
          lastName: '',
          surname: '',
          phone: '',
          work: '',
          gender: '',
          size: '',
          comment: '',
          skills: [],
        })
      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.profileForm.setValue(value);
    });
  }

  fieldHasError(fieldName: string, errorType: string){
    const field = this.profileForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addSkills(){

    if (this.isEdit){
      this.isClick = false;
      this.registers.skills.forEach( (item: Skill) => {
        const skills = <FormArray>this.profileForm.get('skills');
        const skillGroup = new FormGroup({
          skill: new FormControl(`${item.skill}`, Validators.required),
          level: new FormControl(`${item.level}`, Validators.required)
        });
        skills.push(skillGroup);
      })

    } else {
      this.isClick = false;
      const skills = <FormArray>this.profileForm.get('skills');
      const skillGroup = new FormGroup({
        skill: new FormControl(``, Validators.required),
        level: new FormControl(``, Validators.required)
      });
      skills.push(skillGroup);
    }

  }

  getSkillControls(){
    const skills = <FormArray>this.profileForm.get('skills');
    return skills.controls;
  }

  onSubmit(){
    const id = this.editedId || Math.random().toString();
    const form = new Register(id, this.profileForm.value.name,
      this.profileForm.value.lastName, this.profileForm.value.surname,
      this.profileForm.value.phone, this.profileForm.value.work,this.profileForm.value.gender,
      this.profileForm.value.size, this.profileForm.value.comment, this.profileForm.value.skills);
    const next = () => {
      this.registerService.fetchRegisters();
      void this.router.navigate(['/app-register'], {relativeTo: this.route});
    };

    if (this.isEdit) {
      this.registerService.editRegister(form).subscribe(next)
    } else {
      this.registerService.getForm(form).subscribe(next);
    }
  }

  ngOnDestroy(): void {
    this.registerUpLoadingSubscription.unsubscribe();
  }

}

