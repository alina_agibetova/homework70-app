import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';
import { Register } from '../register.model';
import { RegisterService } from '../register.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterResolverService implements Resolve<Register>{

  constructor(private registerService: RegisterService, private router: Router, private http: HttpClient) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Register> | Observable<never> {
    const id = <string>route.params['id'];
    return this.registerService.fetchRegister(id).pipe(mergeMap(register => {
      if (register){
        return of (register);
      }
      void this.router.navigate(['/']);
      return EMPTY;
    }));
  }
}
