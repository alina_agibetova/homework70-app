import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Register, Skill } from '../register.model';
import { Subscription } from 'rxjs';
import { RegisterService } from '../register.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit, OnDestroy {
  registers: Register[] = [];
  isRemoving = false;
  isFetching = false;
  registerChangeSubscription!: Subscription;
  registerFetchingSubscription!: Subscription;
  registerRemovingSubscription!: Subscription;


  constructor(private registerService: RegisterService, private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.registers = this.registerService.getRegister();
    this.registerChangeSubscription = this.registerService.registerChange.subscribe((registers: Register[]) => {
      this.registers = registers;
    });

    this.registerFetchingSubscription = this.registerService.registerFetching.subscribe((isFetching: boolean)=> {
      this.isFetching = isFetching;
    });
    this.registerRemovingSubscription = this.registerService.registerRemoving.subscribe((isRemoving: boolean)=> {
      this.isRemoving = isRemoving;
    });

    this.registerService.fetchRegisters();
  }

  onRemove(id: string){
    this.registerService.removeRegister(id).subscribe(()=> {
      this.registerService.fetchRegisters();
      void this.router.navigate(['/'], {relativeTo: this.route});
    });
  }

  ngOnDestroy(): void {
    this.registerChangeSubscription.unsubscribe();
    this.registerFetchingSubscription.unsubscribe();
    this.registerRemovingSubscription.unsubscribe();
  }

}

