export class Register {
  constructor(
    public id: string,
    public name: string,
    public lastName: string,
    public surname: string,
    public work: string,
    public phone: string,
    public gender: string,
    public size: string,
    public comment: string,
    public skills: Skill[],
  ){}
}

export  class Skill {
  constructor(
    public skill: string,
    public level: string
  ){}
}
